<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function adminIndex()
    {
        if(auth()->user()->hasRole('admin')){
            $users = User::with('roles')->get();
            return view('user.admin.index', compact('users'));
        } else {
            abort(418);
        }
    }
}
