<?php

namespace App\Http\Controllers;

use App\Models\Post;

class PostsController extends Controller
{

    public function index()
    {
        $posts = Post::published()->with('author')->get();
        return view('posts.index', compact('posts'));
    }

    public function adminIndex()
    {
        $posts = null;
        if(auth()->user()->hasAnyRole('admin', 'reviewer')) {
            $posts = Post::all();
        } else {
            $posts = auth()->user()->posts()->get();
        }
        return view('posts.admin.index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }
}
