@extends('user.admin.layouts.app')

@section('main-content')
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{ucfirst($user->getRoleNames()->first())}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
