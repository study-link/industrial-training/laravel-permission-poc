<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel Permission POC</title>
    <link rel="stylesheet" href="{{asset('lib/bootstrap/bootstrap.min.css')}}">
    @stack('styles')
</head>
<body>
@include('posts.admin.layouts.partials._navbar')
<div class="container">
@yield('main-content')
</div>
<script src="{{asset('lib/bootstrap/bootstrap.bundle.min.js')}}"></script>
@stack('scripts')
</body>
</html>
