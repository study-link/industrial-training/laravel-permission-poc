@extends('posts.layouts.app')

@section('main-content')
        <div class="card m-5">
            <h5 class="card-header">{{$post->title}}</h5>
            <div class="card-body">
                <p>
                    {{$post->body}}
                </p>
            </div>
        </div>
@endsection
