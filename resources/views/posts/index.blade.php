@extends('posts.layouts.app')

@section('main-content')
    @auth()
        <h2 class="mt-4">Welcome, {{auth()->user()->name}}</h2>
    @endauth
    <div class="row mb-4">
        @foreach($posts as $post)
            <div class="col-md-4 mb-3">
                <div class="card" >
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{$post->author->name}} | {{\Carbon\Carbon::createFromTimestamp(strtotime($post->published_at))->diffForHumans()}}</h6>
                        <p class="card-text">{{\Illuminate\Support\Str::limit($post->body, 30)}}</p>
                        <a href="{{route('posts.show', $post)}}" class="card-link">See More</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
