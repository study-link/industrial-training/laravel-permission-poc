<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="#">Laravel Permission POC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{route('posts.adminIndex')}}" class="nav-link">Posts</a>
                </li>
                @hasrole('admin')
                <li class="nav-item">
                    <a href="{{route('users.adminIndex')}}" class="nav-link">Users</a>
                </li>
                @endhasrole
                <li class="nav-item">
                    <form action="{{route('logout')}}" method="POST">
                        @csrf
                        <button type="submit" class="nav-link" href="{{route('logout')}}">Logout</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>
