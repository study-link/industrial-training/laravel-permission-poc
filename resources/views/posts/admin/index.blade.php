@extends('posts.admin.layouts.app')

@section('main-content')
    @can('write post')
    <div class="row m-5">
        <div class="col-12 d-flex justify-content-end">
           <button class="btn btn-success">Create New Post</button>
        </div>
    </div>
    @endcan
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Author</th>
                <th>Title</th>
                <th>Body</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->author->name}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{\Illuminate\Support\Str::limit($post->body, 30)}}</td>
                    <td>
                        @can('edit post')
                            <button class="btn btn-warning">Edit</button>
                        @endcan
                        @can('delete post')
                            <button class="btn btn-danger">Delete</button>
                        @endcan
                        @can('change visibility of post')
                            @if($post->published_at != null)
                                <button class="btn btn-info">Hide</button>
                            @else
                                <button class="btn btn-info">Show</button>
                            @endif
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
