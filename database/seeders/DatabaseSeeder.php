<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Initializing Roles and permissions
        $roles = [
            Role::findOrCreate('admin'),
            Role::findOrCreate('reviewer'),
            Role::findOrCreate('writer'),
        ];

        $postPermissions = [
            Permission::findOrCreate('write post'),
            Permission::findOrCreate('edit post'),
            Permission::findOrCreate('delete post'),
            Permission::findOrCreate('change visibility of post'),
        ];

        $userPermissions = [
            Permission::findOrCreate('promote user'),
            Permission::findOrCreate('demote user'),
        ];

        $roles[0]->syncPermissions(array_merge($postPermissions, $userPermissions));
        $roles[1]->syncPermissions([$postPermissions[2], $postPermissions[3]]);
        $roles[2]->syncPermissions([$postPermissions[0], $postPermissions[1]]);

        // Creating Admin User and assigning Role of admin
        $user = User::factory()->create([
            'name' => 'Administrator',
            'email' => 'admin@example.com',
            'password' => Hash::make('abcd1234'),
        ]);
        $user->assignRole('admin');

        // Creating Reviewer User
        $user = User::factory()->create([
            'name' => 'Reviewer',
            'email' => 'reviewer@example.com',
            'password' => Hash::make('abcd1234'),
        ]);
        $user->assignRole('reviewer');

        // Creating 2 Writers
        $user = User::factory()->create([
            'name' => 'Writer 1',
            'email' => 'writer1@example.com',
            'password' => Hash::make('abcd1234'),
        ]);
        $user->assignRole('writer');

        $user = User::factory()->create([
            'name' => 'Writer 2',
            'email' => 'writer2@example.com',
            'password' => Hash::make('abcd1234'),
        ]);
        $user->assignRole('writer');

        Post::factory(10)->create();
    }
}
