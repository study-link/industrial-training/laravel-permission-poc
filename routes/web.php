<?php

use App\Http\Controllers\PostsController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('posts.index');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/posts', [PostsController::class, 'index'])->name('posts.index');
Route::get('/posts/{post:slug}', [PostsController::class, 'show'])->name('posts.show');

Route::middleware('auth')->prefix('admin')->group(function () {
   Route::get('/posts', [PostsController::class, 'adminIndex'])->name('posts.adminIndex');
   Route::get('/users', [\App\Http\Controllers\UsersController::class, 'adminIndex'])->name('users.adminIndex');
});

require __DIR__.'/auth.php';
